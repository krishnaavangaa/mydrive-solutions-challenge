//
//  Constant.h
//  MyDriveProduct
//
//  Created by Krishna Vanga on 19/06/2015.
//  Copyright (c) 2015 MyDrive Solutions. All rights reserved.
//

#ifndef MyDriveProduct_Constant_h
#define MyDriveProduct_Constant_h

#define kExchangeRatesEndPoint    @"https://raw.githubusercontent.com/mydrive/code-tests/master/iOS-currency-exchange-rates/rates.json"

#define kConversation     @"conversions"
#define kExchangeFrom     @"from"
#define kExchangeTo       @"to"
#define kExchangeRate     @"rate"

#define kBaseCurrency    @"USD"


#endif
