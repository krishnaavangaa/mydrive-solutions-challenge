//
//  Exchange.m
//  MyDriveProduct
//
//  Created by Krishna Vanga on 19/06/2015.
//  Copyright (c) 2015 MyDrive Solutions. All rights reserved.
//

#import "Exchange.h"
#import "AppDelegate.h"

#import "Conversion.h"


@implementation Exchange

@dynamic toValue;
@dynamic fromValue;
@dynamic rate;

#pragma mark - Methods

+(void)insertDataWith:(Conversion*)exchangeRate {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    Exchange *exchange = [NSEntityDescription insertNewObjectForEntityForName:@"Exchange"
                                                       inManagedObjectContext:context];
    
    exchange.fromValue = exchangeRate.fromCurrency;
    exchange.toValue = exchangeRate.toCurrency;
    exchange.rate =  exchangeRate.rate;
    
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
}

+ (NSArray*)getlistofRates {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSArray *ratesArrary = nil;
    if (context != nil){
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Exchange"];
        ratesArrary = [context executeFetchRequest:fetchRequest error:nil];
    }
    return ratesArrary;
}

+ (void)deleteAllRates {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    NSFetchRequest *allRates = [[NSFetchRequest alloc] init];
    [allRates setEntity:[NSEntityDescription entityForName:@"Exchange" inManagedObjectContext:context]];
    [allRates setIncludesPropertyValues:NO];
    
    NSError *error = nil;
    NSArray *rates = [context executeFetchRequest:allRates error:&error];
    for (Exchange *exchange  in rates) {
        [context deleteObject:exchange];
    }
    NSError *saveError = nil;
    [context save:&saveError];
}

@end
