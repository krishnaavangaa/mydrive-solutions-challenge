//
//  Exchange.h
//  MyDriveProduct
//
//  Created by Krishna Vanga on 19/06/2015.
//  Copyright (c) 2015 MyDrive Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Conversion.h"


@interface Exchange : NSManagedObject

@property (nonatomic, retain) NSString * toValue;
@property (nonatomic, retain) NSString * fromValue;
@property (nonatomic, retain) NSNumber * rate;

+(void)insertDataWith:(Conversion*)exchangeRate;
+ (NSArray*)getlistofRates;
+ (void)deleteAllRates;

@end
