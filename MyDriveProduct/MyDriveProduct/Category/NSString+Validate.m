//
//  NSString+Validate.m
//  MyDriveProduct
//
//  Created by Krishna Vanga on 19/06/2015.
//  Copyright (c) 2015 MyDrive Solutions. All rights reserved.
//

#import "NSString+Validate.h"

@implementation NSString (Validate)

- (BOOL)isValidate {
    
    if ([self length] != 0) {
        return YES;
    }
    else {
        return NO;
    }
    
    return YES;
}

@end
