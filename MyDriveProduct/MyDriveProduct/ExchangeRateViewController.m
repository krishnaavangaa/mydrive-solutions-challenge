//
//  ViewController.m
//  MyDriveProduct
//
//  Created by Krishna Vanga on 19/06/2015.
//  Copyright (c) 2015 MyDrive Solutions. All rights reserved.
//

#import "ExchangeRateViewController.h"
#import "Conversion.h"
#import "ExchangeManager.h"

@interface ExchangeRateViewController ()<UIPickerViewDataSource,UIPickerViewDelegate>

@property (nonatomic ,strong) NSArray *exchangeArray;
@property (nonatomic ,strong) ExchangeManager *manager;
@property (nonatomic ,strong) NSString *fromExchange;
@property (nonatomic ,strong) NSString *toExchange;
@property (nonatomic ,strong) NSNumber *rateValue;
@property (nonatomic ,assign) BOOL isFromExchangeSelected;

@property (nonatomic ,weak) IBOutlet UIButton *fromExchangeButton;
@property (nonatomic ,weak) IBOutlet UIButton *toExchangeButton;
@property (nonatomic ,weak) IBOutlet UIButton *calculateRateButton;
@property (nonatomic ,weak) IBOutlet UITextField *textField;

@property (nonatomic ,weak) IBOutlet UILabel *outPutLabel;
@property (nonatomic ,weak) IBOutlet UILabel *fromConvLabel;
@property (nonatomic ,weak) IBOutlet UILabel *toConvLabel;

@property (nonatomic ,weak) IBOutlet UIPickerView *pickerView;

@end

@implementation ExchangeRateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.exchangeArray = [NSArray array];
    self.manager = [ExchangeManager sharedInstance];
    [self resetValues];
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIPicker Data Source and Delegate


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return [self.exchangeArray count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSString *title = self.exchangeArray[row];
    
    return title;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    NSString *value = self.exchangeArray[row];
    
    if (self.isFromExchangeSelected == YES) {
        self.fromExchange = value;
        [self.fromExchangeButton setTitle:value
                                 forState:UIControlStateNormal];
    }
    else {
        self.toExchange = value;
        [self.toExchangeButton setTitle:self.toExchange forState:UIControlStateNormal];
    }
    [self.pickerView setHidden:YES];
}

#pragma mark - Actions

- (IBAction)fromExchangeButtonPressed:(id)sender {
    [self.pickerView setHidden:NO];
    self.isFromExchangeSelected = YES;
    self.exchangeArray = [self.manager findAllCurrency];
    [self.pickerView reloadAllComponents];
}
- (IBAction)toExchangeButtonPressed:(id)sender {
    [self.pickerView setHidden:NO];
    if ([self.fromExchange length] != 0) {
        self.isFromExchangeSelected = NO;
        NSMutableArray *arrary = [self.manager findAllCurrency];
        [arrary removeObject:self.fromExchange];
        self.exchangeArray = arrary;
        [self.pickerView reloadAllComponents];
    }
    else {
        [self showAlertWith:@"Select" andMessage:@"From Currency"];
    }
}

- (IBAction)submitExchangeButtonPressed:(id)sender {
    
    NSNumber* calcualteValue = [self.manager exchangeRateFrom:self.fromExchange andTo:self.toExchange];
    double value = [calcualteValue doubleValue];
    
    int amount = [self.textField.text intValue];
    NSString *totalString = [NSString stringWithFormat:@" %d %@ = %.02f %@",amount,self.fromExchange,amount*value,self.toExchange];
    NSString *fromValueString = [NSString stringWithFormat:@" 1 %@ = %.02f %@",self.fromExchange,value,self.toExchange];
        NSString *toValueString = [NSString stringWithFormat:@" 1 %@ = %.02f %@",self.toExchange,1/value,self.fromExchange];
    
    [self.outPutLabel setText:totalString];
    [self.fromConvLabel setText:fromValueString];
    [self.toConvLabel setText:toValueString];
}

- (IBAction)restButtonPressed:(id)sender
{
    [self resetValues];
}

#pragma mark - Method
- (void)resetValues {
    [self.fromExchangeButton setTitle:@"GBP" forState:UIControlStateNormal];
    self.fromExchange = @"GBP";
    [self.toExchangeButton setTitle:@"USD" forState:UIControlStateNormal];
    self.toExchange = @"USD";
    [self.outPutLabel setText:@""];
    [self.fromConvLabel setText:@""];
    [self.toConvLabel setText:@""];
    
    [self.pickerView setHidden:YES];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.textField resignFirstResponder];
}

#pragma mark - PopUp

- (void)showAlertWith:(NSString*)title
           andMessage:(NSString*)message {
    
    UIAlertController *alertViewController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
    
    [alertViewController addAction:alertAction];
    
    [self presentViewController:alertViewController animated:YES completion:nil];
}
@end
