//
//  ExchangeManager.h
//  MyDriveProduct
//
//  Created by Krishna Vanga on 22/06/2015.
//  Copyright (c) 2015 MyDrive Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ExchangeRateDelegate.h"

@interface ExchangeManager : NSObject <ExchangeRateDelegate>
@property (nonatomic ,strong) NSArray *exchangeRatesArray;

+(instancetype)sharedInstance;
- (void)getExchangeRates;

@end
