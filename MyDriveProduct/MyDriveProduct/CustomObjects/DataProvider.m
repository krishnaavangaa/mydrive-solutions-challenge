//
//  DataProvider.m
//  MyDriveProduct
//
//  Created by Krishna Vanga on 19/06/2015.
//  Copyright (c) 2015 MyDrive Solutions. All rights reserved.
//

#import "DataProvider.h"
#import "Constant.h"

@implementation DataProvider

#pragma mark - DataDelegte

- (void)downloadDataWith:(ResutlBlock)block {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,0), ^(void) {
        NSMutableData *receivedData;
        NSString *loginURL= kExchangeRatesEndPoint;
        
        NSURLRequest *loginURLRequest=[NSURLRequest requestWithURL:[NSURL URLWithString:loginURL]];
        __block NSError *errors;
        receivedData=[NSMutableData dataWithData:[NSURLConnection sendSynchronousRequest:loginURLRequest returningResponse:nil
                                                                                   error:&errors]];
        dispatch_async(dispatch_get_main_queue(),^ {
            NSError *error;
            if(receivedData){
                NSDictionary *response=[NSJSONSerialization JSONObjectWithData:receivedData options:kNilOptions error:&error];
                
                if(error){
                    
                    NSLog(@"Serialization error: %@", error.localizedDescription);
                    
                    block(nil,error);
                }
                else {
                    NSArray *result=[response objectForKey:kConversation];
                    NSLog(@"response from server %@",result);
                    block(result,nil);
                }
                
            }
            else {
                NSLog(@"Serialization error: %@", error.localizedDescription);
            }
            
        });
    });
}


@end
