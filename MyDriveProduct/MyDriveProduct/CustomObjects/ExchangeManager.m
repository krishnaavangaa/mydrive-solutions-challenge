//
//  ExchangeManager.m
//  MyDriveProduct
//
//  Created by Krishna Vanga on 22/06/2015.
//  Copyright (c) 2015 MyDrive Solutions. All rights reserved.
//

#import "ExchangeManager.h"
#import "Conversion.h"
#import "DataProvider.h"
#import "Exchange.h"
#include "NSString+Validate.h"
#include "Constant.h"

@interface ExchangeManager ()

@property (nonatomic ,strong) DataProvider *dataProvider;

@end

@implementation ExchangeManager

#pragma mark - Singleton Creation

+(instancetype)sharedInstance {
    
    static ExchangeManager *serviceManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once (&onceToken, ^{
        serviceManager = [[self alloc] init];
        
    });
    return serviceManager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.exchangeRatesArray = [[NSArray alloc]init];
        self.dataProvider = [[DataProvider alloc] init];
    }
    return self;
}

#pragma mark - ExchnageRates Delegate

- (NSArray*)findAllExchangeRates {
    return self.exchangeRatesArray;
}

- (NSMutableArray*)findAllCurrency {
    NSMutableArray *allCurrency = [NSMutableArray array];
    for (Conversion *exchange in self.exchangeRatesArray) {
        if ([allCurrency containsObject:exchange.fromCurrency] == NO) {
            [allCurrency addObject:exchange.fromCurrency];
        }
        if ([allCurrency containsObject:exchange.toCurrency] == NO) {
            [allCurrency addObject:exchange.toCurrency];
        }
    }
    
    return allCurrency;
}

- (NSArray*)findallFromCurrency {
    NSMutableArray *allFromArray = [NSMutableArray array];
    for (Conversion *exchange in self.exchangeRatesArray) {
            if ([allFromArray containsObject:exchange.fromCurrency] == NO) {
                [allFromArray addObject:exchange.fromCurrency];
        }
    }
    return allFromArray;
}
- (NSArray*)findAllTo:(NSString*)fromValue {
    NSMutableArray *toArray = [NSMutableArray array];
    for (Conversion *exchange in self.exchangeRatesArray) {
        if ([exchange.fromCurrency isEqualToString:fromValue]) {
            if ([toArray containsObject:exchange] == NO) {
                [toArray addObject:exchange];
            }
        }
    }
    return toArray;
}

- (NSArray*)findAllFrom:(NSString*)toValue {
    NSMutableArray *fromArray = [NSMutableArray array];
    for (Conversion *exchange in self.exchangeRatesArray) {
        if ([exchange.toCurrency isEqualToString:toValue]) {
            if ([fromArray containsObject:exchange] == NO) {
                [fromArray addObject:exchange];
            }
        }
    }
    return fromArray;
}

- (NSNumber*)exchangeRateFrom:(NSString*)fromValue
                       andTo:(NSString*)toValue {
    NSNumber *exchangeRateValue = nil;
    for (Conversion *exchange in self.exchangeRatesArray) {
        if ([exchange.toCurrency isEqualToString:toValue] && [exchange.fromCurrency isEqualToString:fromValue]) {
            exchangeRateValue = exchange.rate;
            return exchangeRateValue;
        }
        
        else if([exchange.toCurrency isEqualToString:fromValue] && [exchange.fromCurrency isEqualToString:toValue]) {
            double rateCurrency  = 1 /[exchange.rate doubleValue];
            exchangeRateValue = [NSNumber numberWithDouble:rateCurrency];
            
            return exchangeRateValue;
        }
    }
    
    double fromRateCurrency = 0.0;
    double toRateCurrency = 0.0;
    
    fromRateCurrency = [self findBaseCurrency:fromValue];
    
    toRateCurrency = [self findBaseCurrency:toValue];
    
    double rateCurrency  = fromRateCurrency /toRateCurrency;
    
    exchangeRateValue = [NSNumber numberWithDouble:rateCurrency];
    
    return exchangeRateValue;
}

#pragma mark - CoreData

- (void)updateExchangeCoreData {
    [Exchange deleteAllRates];
    for (Conversion *exchange in self.exchangeRatesArray) {
        [Exchange insertDataWith:exchange];
    }
}

#pragma mark - Methods
- (void)getExchangeRates {
    __weak ExchangeManager *weakSelf = self;
    
    [weakSelf.dataProvider downloadDataWith:
     ^(NSArray *exchangeArray, NSError *error) {
         [weakSelf convertToExchangeData:exchangeArray];
     }];
    
    [self updateExchangeCoreData];
}

- (void)convertToExchangeData:(NSArray*)array {
    NSMutableArray *exchnangeArray = [NSMutableArray array];
    
    for (NSDictionary *dict  in array) {
        NSString * fromValue = [dict objectForKey:kExchangeFrom];
        NSString * tovalue = [dict objectForKey:kExchangeTo];
        NSNumber *rateValue = [dict objectForKey:kExchangeRate];
        
        if ([fromValue isValidate]| [tovalue isValidate]) {
            
            Conversion *exchange = [[Conversion alloc] initWithFrom:fromValue
                                                                     To:tovalue andRate:rateValue];
            [exchnangeArray addObject:exchange];
        }
    }
    self.exchangeRatesArray = exchnangeArray;
}

- (double)findBaseCurrency:(NSString*)value {
    float  baseCurrecy = 0.0 ;
    
    for (Conversion *exchange in self.exchangeRatesArray) {
        if ([exchange.fromCurrency isEqual:value] && [exchange.toCurrency isEqual:kBaseCurrency]) {
            baseCurrecy = [exchange.rate doubleValue];
        }
        if ([exchange.fromCurrency isEqual:kBaseCurrency] && [exchange.toCurrency isEqual:value]) {
            
            baseCurrecy = 1/[exchange.rate doubleValue];
        }
    }
    return baseCurrecy;
}

@end
