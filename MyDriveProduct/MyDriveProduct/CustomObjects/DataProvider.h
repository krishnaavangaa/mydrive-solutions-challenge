//
//  DataProvider.h
//  MyDriveProduct
//
//  Created by Krishna Vanga on 19/06/2015.
//  Copyright (c) 2015 MyDrive Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataDownloadDelegate.h"

@interface DataProvider : NSObject <DataDownloadDelegate>


@end
