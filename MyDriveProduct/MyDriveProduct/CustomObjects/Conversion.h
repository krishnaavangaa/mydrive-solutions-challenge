//
//  Conversion.h
//  MyDriveProduct
//
//  Created by Krishna Vanga on 19/06/2015.
//  Copyright (c) 2015 MyDrive Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Conversion : NSObject

@property (nonatomic ,copy,readonly) NSString *fromCurrency;
@property (nonatomic ,copy,readonly) NSString *toCurrency;
@property (nonatomic ,copy,readonly) NSNumber *rate;


- (instancetype)initWithFrom:(NSString*)fromValue
                          To:(NSString*)toValue
                     andRate:(NSNumber*)rate;

@end
