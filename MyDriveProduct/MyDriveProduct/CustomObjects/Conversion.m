//
//  Conversion.m
//  MyDriveProduct
//
//  Created by Krishna Vanga on 19/06/2015.
//  Copyright (c) 2015 MyDrive Solutions. All rights reserved.
//

#import "Conversion.h"

@interface Conversion ()

@property (nonatomic ,copy,readwrite) NSString *fromCurrency;
@property (nonatomic ,copy,readwrite) NSString *toCurrency;
@property (nonatomic ,copy,readwrite) NSNumber *rate;

@end

@implementation Conversion

- (instancetype)initWithFrom:(NSString*)fromValue
                       To:(NSString*)toValue
                     andRate:(NSNumber*)rate
{
    self = [super init];
    if (self) {
        self.fromCurrency = fromValue;
        self.toCurrency = toValue;
        self.rate = rate;
    }
    
    return self;
}



@end
