//
//  DataDownloadDelegate.h
//  MyDriveProduct
//
//  Created by Krishna Vanga on 22/06/2015.
//  Copyright (c) 2015 MyDrive Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ResutlBlock) (NSArray *exchangeArray,NSError*error);

@protocol DataDownloadDelegate <NSObject>

- (void)downloadDataWith:(ResutlBlock)block;

@end
