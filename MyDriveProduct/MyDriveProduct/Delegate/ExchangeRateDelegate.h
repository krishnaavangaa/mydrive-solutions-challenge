//
//  ExchangeRateDelegate.h
//  MyDriveProduct
//
//  Created by Krishna Vanga on 22/06/2015.
//  Copyright (c) 2015 MyDrive Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ExchangeRateDelegate <NSObject>

- (NSArray*)findAllExchangeRates;
- (NSArray*)findAllTo:(NSString*)fromValue;
- (NSArray*)findAllFrom:(NSString*)toValue;
- (NSNumber*)exchangeRateFrom:(NSString*)fromValue
                        andTo:(NSString*)toValue;
- (NSArray*)findallFromCurrency;
- (NSMutableArray*)findAllCurrency;



@end
