//
//  ExchangeMangerTests.m
//  MyDriveProduct
//
//  Created by Krishna Vanga on 22/06/2015.
//  Copyright (c) 2015 MyDrive Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "ExchangeManager.h"
#import "Conversion.h"

@interface ExchangeMangerTests : XCTestCase

@property (nonatomic,strong) ExchangeManager *manager;

@end

@implementation ExchangeMangerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    self.manager = [self getSharedInstance];
    self.manager.exchangeRatesArray = [self createCustomExchangeRates];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

#pragma mark - Helper
- (ExchangeManager *)getSharedInstance {
    return [ExchangeManager sharedInstance];
}

- (NSArray*)createCustomExchangeRates {
    
    Conversion *exchange1 = [[Conversion alloc] initWithFrom:@"EUR"
                                                              To:@"USD" andRate:@1.09833];
    
    Conversion *exchange2 = [[Conversion alloc] initWithFrom:@"USD"
                                                              To:@"JPY"
                                                         andRate:@123.779];
    
    
    Conversion *exchange3 = [[Conversion alloc] initWithFrom:@"GBP"
                                                              To:@"USD"
                                                         andRate:@1.52967];
    
    
    Conversion *exchange4 = [[Conversion alloc] initWithFrom:@"USD"
                                                              To:@"CHF"
                                                         andRate:@0.94159];
    
    Conversion *exchange5 = [[Conversion alloc] initWithFrom:@"USD"
                                                              To:@"CAD"
                                                         andRate:@1.24333];
    
    Conversion *exchange6 = [[Conversion alloc] initWithFrom:@"EUR"
                                                              To:@"JPY"
                                                         andRate:@135.950];
    
    
    NSArray *ratesArray = @[exchange1,exchange2,exchange3,exchange4,exchange5,exchange6];
    
    return ratesArray;
    
}

#pragma mark -Tests

- (void)testSingletonSharedInstanceCreated {
    XCTAssertNotNil(self.manager);
}

- (void)testAllRatesFromValue {
    NSArray *toArray =[self.manager findAllTo:@"USD"];
    
    XCTAssertTrue( [toArray count] == 3, @"We should have three exchange rates from USD" );
}

- (void)testAllRatesToValue {
    NSArray *fromArray =[self.manager findAllFrom:@"JPY"];
    
    XCTAssertTrue( [fromArray count] == 2, @"We should have two exchange rates" );
}

- (void)testCaseEURToJYP {
    
    NSNumber *rate = [self.manager exchangeRateFrom:@"EUR"
                                                    andTo:@"JPY"];
    
    XCTAssertTrue([rate isEqualToNumber:@135.950] == YES, @"Exchange Rate must be 135.950" );

}

- (void)testCaseJYPToEUR {
    NSNumber *rate = [self.manager exchangeRateFrom:@"JPY"
                                              andTo:@"EUR"];
      XCTAssertTrue([rate isEqualToNumber:@(1/135.950)] == YES, @"Exchange Rate must be 0.00735");
}

- (void)testCaseGBPToJYP {
    
    NSNumber *rate = [self.manager exchangeRateFrom:@"GBP"
                                                    andTo:@"JPY"];
    
    XCTAssertTrue(rate != nil, @"Exchange Rate must be nil" );
}

- (void)testCaseJPYToGBP {
    NSNumber *rate = [self.manager exchangeRateFrom:@"JPY"
                                                    andTo:@"GBP"];
    
    XCTAssertTrue(rate != nil, @"Exchange Rate must be nill" );
}

@end
